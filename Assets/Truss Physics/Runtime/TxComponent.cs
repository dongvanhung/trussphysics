﻿/* ______                   ___  __            _       
  /_  __/_____ _____ ___   / _ \/ /  __ _____ (_)______
   / / / __/ // (_-<(_-<  / ___/ _ \/ // (_-</ / __(_-<
  /_/ /_/  \_,_/___/___/ /_/  /_//_/\_, /___/_/\__/___/
  Soft-Body Simulation for Unity3D /___/               
                                         Heartbroken :( */

using UnityEngine;

public class TxComponent : MonoBehaviour
{
    #region Properties

    public bool isValid
    {
        get { return m_valid; }
    }

    #endregion

    #region Events

    public delegate void OnBeforeStepFn();
    public event OnBeforeStepFn onBeforeStep;

    public delegate void OnAfterStepFn();
    public event OnAfterStepFn onAfterStep;

    public delegate void OnAfterUpdateFn();
    public event OnAfterUpdateFn onAfterUpdate;

    #endregion

    #region Unity

    void OnEnable()
    {
        TxPhysics.world.AddComponent();
    }

    void OnDisable()
    {
        Destroy();
        TxPhysics.world.RemoveComponent();
    }

    #endregion

    #region Methods

    public virtual void Create()
    {
        TxBody parentBody = (this is TxBody) ? (this as TxBody).FindParentBody() : (this is TxConstraint) ? (this as TxConstraint).attachedBody : null;
        if (parentBody != null)
        {
            parentBody.onBeforeStep += OnBeforeStep;
            parentBody.onAfterStep += OnAfterStep;
            parentBody.onAfterUpdate += OnAfterUpdate;
        }
        else
        {
            TxPhysics.onBeforeStep += OnBeforeStep;
            TxPhysics.onAfterStep += OnAfterStep;
            TxPhysics.onAfterUpdate += OnAfterUpdate;
        }

        m_valid = true;
    }

    public virtual void Destroy()
    {
        TxBody parentBody = (this is TxBody) ? (this as TxBody).FindParentBody() : (this is TxConstraint) ? (this as TxConstraint).attachedBody : null;
        if (parentBody != null)
        {
            parentBody.onBeforeStep -= OnBeforeStep;
            parentBody.onAfterStep -= OnAfterStep;
            parentBody.onAfterUpdate -= OnAfterUpdate;
        }
        else
        {
            TxPhysics.onBeforeStep -= OnBeforeStep;
            TxPhysics.onAfterStep -= OnAfterStep;
            TxPhysics.onAfterUpdate -= OnAfterUpdate;
        }

        m_valid = false;
    }

    #endregion

    #region Protected

    protected virtual void OnBeforeStep()
    {
        if (onBeforeStep != null) onBeforeStep();
    }
    protected virtual void OnAfterStep()
    {
        if (onAfterStep != null) onAfterStep();
    }
    protected virtual void OnAfterUpdate()
    {
        if (onAfterUpdate != null) onAfterUpdate();
    }

    #endregion

    #region Private

    bool m_valid = false;

    #endregion
}
